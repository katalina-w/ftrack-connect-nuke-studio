..
    :copyright: Copyright (c) 2014 ftrack

*****
Using
*****

Find out all information you need to use the nuke studio with ftrack.

.. toctree::
    :maxdepth: 1

    launching
    exporting_project
    processors
    templates
    info_panel
    crew

.. note::

    In order for the Nuke studio plugin to work properly you will not to have
    Shot, Sequence and Episode objects available on the :term:`ftrack server`.
