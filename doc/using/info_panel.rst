..
    :copyright: Copyright (c) 2015 ftrack

**********
Info panel
**********

To get more information about a shot in ftrack from a corresponding track item
you can use the info panel. Open the info panel from the ftrack menu:

.. image:: /image/open_info_panel.png

The info panel will show info, notes and activities for the related shot in
ftrack:

.. image:: /image/info_panel.png


.. note::

    The track item must have a related shot in ftrack that has been created or
    updated using the export dialog.
